//testing LCD i2c, ds1307, DS18B20 1-wire, volt sensor - all working, can be implements in engineering work
//need to make gsm lib with sms send/recive only

#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <DS1307.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
DS1307 clock;
RTCDateTime dt;
OneWire oneWire(A0);
DallasTemperature sensors(&oneWire);

const byte cooler = 2;
const byte vSensor1 = A1; 

void setup()
{
  lcd.begin(16,2);
  Serial.begin(9600);

  sensors.begin();
  lcd.backlight();

  pinMode(cooler, OUTPUT);

  clock.begin();
  if (!clock.isReady())
  {
    clock.setDateTime(__DATE__, __TIME__);
  }
}

void loop()
{
  digitalWrite(cooler, LOW);
  dt = clock.getDateTime();

  sensors.requestTemperatures();

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(dt.hour);     lcd.print(":");
  lcd.print(dt.minute);   lcd.print(":");
  lcd.print(dt.second);   lcd.print("");
  Serial.print(dt.hour);  Serial.print(":");
  Serial.print(dt.minute);Serial.print(":");
  Serial.print(dt.second);Serial.println(" ");

  lcd.setCursor(0,1);
  lcd.print("sys T: ");
  lcd.print(sensors.getTempCByIndex(0));

  delay(1000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(dt.year);     lcd.print(".");
  lcd.print(dt.month);    lcd.print(".");
  lcd.print(dt.day);      
  lcd.setCursor(0,1);
  lcd.print("sys V: ");
  lcd.print(vMeasurement(vSensor1));
  Serial.println(vMeasurement(vSensor1));
  delay(1000);
}

float vMeasurement(byte vSensor)
{
  float v = analogRead(vSensor);
  v = ((v*5)/1024);
  v = v/0.2;
  return (v);
}
