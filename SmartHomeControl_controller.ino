//Smart Home Controller with alarm system by Kamil Trzcinski
//for use with DFRobot0355 with Leonardo mainboard,
//4x PIR(nc) sensor, 3x DS18B20 sensor, 1x MQ-7 sensor,
//RTC1307,

//CREATED FOR ENGINEERING WORK BY KAMIL TRZCINSKI 2019


//think HIGH/LOW for single pir
//MQ-7 - need to do
//GSM need to do own gsm lib with send/recive sms only

#include <Wire.h>
#include <DS1307.h>

DS1307 clock;
RTCDateTime dt;

//declaration GSM library
#include <GSM.h>
GSM gsmAccess;
GSM_SMS sms;
volatile char senderNumber[20];
volatile char smsText[3];
volatile byte smsSecondChar;
volatile byte smsThirdChar;
#define PINNUMBER ""

//safe mobile numbers, can arm and disarm alarm system
const char* safeNumbers[] =
{
  "570******"
};
const byte sizeOfSafeNumbers = 1;

//messages for alarm and sabotage - to send via SMS
const char* messages [] =
{
  "ALARM czujka w kuchnii!",        //no 0
  "SABOTAZ czujka w kuchnii",       //no 1
  "ALARM czujka korytarz",          //no 2
  "SABOTAZ czujka korytarz",        //no 3
  "ALARM czujka salon/balkon",      //no 4
  "SABOTAZ czujka salon/balkon",    //no 5
  "ALARM czujka salon/naroznik",    //no 6
  "SABOTAZ czujka salon/naroznik",  //no 7
  "UZBROJONY, nie wchodzic",        //no 8
  "ROZBROJONY, mozna wejsc",        //no 9
  "BRAK 230V",                      //no 10
  "POWROT 230V",                    //no 11
  "AKUMULATOR WYCZERPANY!",         //no 12
  "AKUMULATOR NALADOWANY"           //no 13
};

//PIR sensor should by allways connected to 12 DC
//to disarm system, use boolean
volatile boolean allPirStatus = false;
//volatile boolean pirStatus1 = false;
//volatile boolean pirStatus2 = false;
//volatile boolean pirStatus3 = false;
//volatile boolean pirStatus4 = false;

//DC in and battery status
volatile boolean dcIn = true;
volatile boolean batteryIn = true;

//PIR sensor kitchen, cable no 1
const byte pirSensor1 = 11;
const byte sabotage1 = 10;
//PIR sensor front door, cable no 2
const byte pirSensor2 = 9;
const byte sabotage2 = 8;
//PIR sensor salon above window, cable no 3
const byte pirSensor3 = 7;
const byte sabotage3 = 6;
//PIR sensor salon corner, cable no 4
const byte pirSensor4 = 5;
const byte sabotage4 = 4;

// temp sensor on RTC module - DS18B20 sensor
const byte tempRTC = 4;

//gas sensor MQ-7 near stairs, cable no 2
const byte gasSensor = A5;

//voltage sensors
const byte vSensor1 = A1; // DC in voltage
const byte vSensor2 = A2; // battery voltage
//NEED TO SET UP LOW BATTERY VALUE!!!
const float lowBateryValue = 9.7;

void setup() {
  pinMode(pirSensor1, INPUT);
  pinMode(sabotage1, INPUT);
  pinMode(pirSensor2, INPUT);
  pinMode(sabotage2, INPUT);
  pinMode(pirSensor3, INPUT);
  pinMode(sabotage3, INPUT);
  pinMode(pirSensor4, INPUT);
  pinMode(sabotage4, INPUT);

  Serial.begin(9600);
  while(!Serial);

  clock.begin();
  if (!clock.isReady())
  {
    clock.setDateTime(__DATE__, __TIME__);
  }
  
  // connection state
  boolean notConnected = true;

  // Start GSM connection
  while (notConnected) {
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }
}

void loop() {
  //get data and time from RTC
  dt = clock.getDateTime();
  
  //checking the mains voltage and the battery
  if (vMeasurement(vSensor1) < 7)
  {
    smsSend(safeNumbers[0], messages[10]);
    dcIn = false;
  }
  if (vMeasurement(vSensor2) < lowBateryValue)
  {
    smsSend(safeNumbers[0], messages[12]);
    batteryIn = false;
  }
  if (vMeasurement(vSensor1) >= 14.5 && !dcIn)
  {
    smsSend(safeNumbers[0], messages[11]);
    dcIn = true;
  }
  if (vMeasurement(vSensor2) >= 14.2 && !batteryIn)
  {
    smsSend(safeNumbers[0], messages[13]);
    batteryIn = true;
  }

  //checking alarm status
  alarm(pirSensor1, pirSensor2, pirSensor3, pirSensor4);

  //checking sabotage status
  sabotage(sabotage1, sabotage2, sabotage3, sabotage4);

  //checking serial and recive sms
  smsRecive();
  if (smsText[0] == ',')
  {
    smsSecondChar = (byte)smsText[1];
    smsThirdChar = (byte)smsText[2];
    smsControl(smsSecondChar, smsThirdChar);
  }
}





//if PIR sensor HIGH on pin, send message to owner
void alarm(byte pirSensor1, byte pirSensor2, byte pirSensor3, byte pirSensor4)
{
  if (digitalRead(pirSensor1) == HIGH && allPirStatus)
  {
    smsSend(safeNumbers[0], messages[0]);
  }
  if (digitalRead(pirSensor2) == HIGH && allPirStatus)
  {
    smsSend(safeNumbers[0], messages[2]);
  }
  if (digitalRead(pirSensor3) == HIGH && allPirStatus)
  {
    smsSend(safeNumbers[0], messages[4]);
  }
  if (digitalRead(pirSensor4) == HIGH && allPirStatus)
  {
    smsSend(safeNumbers[0], messages[6]);
  }
}

//if sabotage in PIR sensor HIGH on pin, send message to owner
void sabotage(byte sabotageSensor1, byte sabotageSensor2, byte sabotageSensor3, byte sabotageSensor4)
{
  if (digitalRead(sabotageSensor1) == HIGH)
  {
    smsSend(safeNumbers[0], messages[1]);
  }
  if (digitalRead(sabotageSensor2) == HIGH)
  {
    smsSend(safeNumbers[0], messages[3]);
  }
  if (digitalRead(sabotageSensor3) == HIGH)
  {
    smsSend(safeNumbers[0], messages[5]);
  }
  if (digitalRead(sabotageSensor4) == HIGH)
  {
    smsSend(safeNumbers[0], messages[7]);
  }
}

//measuring voltage - have use 30K and 7,5K resistor - 7,5/(30+7,5)=0,2
float vMeasurement(byte vSensor)
{
  float v = analogRead(vSensor);
  v = ((v*5)/1024)/0,2;
  return (v, 1);
}

//sending text message to owner with system status
void testMessage()
{
  byte h = dt.hour;
  byte m = dt.minute;
  byte s = dt.second;
  if(h == 17 && m == 00)
  {
    if (00 < s && s >30)
    {
      char message = " ";
      smsSendStatus(safeNumbers[0], message);
    }
  }
  //send text with data to owner:
  //everyday at 17:00
  // - system status (is armed, is disarmed)
  // - temp in the RACK cabinet
  // - DC voltage and battery voltage
}

//controling via sms the controller
void smsControl(byte secondChar, byte thirdChar)
{
  switch (secondChar)
  {
    case 0:
      switch (smsThirdChar)
      {
        //armed system
        case 0:
          allPirStatus = true;
          Serial.println("sending...");
          smsSend(senderNumber, 8);
          smsClean();
          Serial.println("send");
          break;
        //disarmed system
        case 1:
          allPirStatus = false;
          smsSend(senderNumber, 9);
          smsClean();
          break;
        //system status
        case 9:
          char sysStat = " ";
          if(allPirStatus)
          {
            sysStat = "System uzbrojony, nie wchodz.\n";
          }
          else
          {
            sysStat = "System rozbrojony, mozesz wejsc.\n";
          }
          char data = (clock.dateFormat("jS M y, G:ia\n", dt));
          //get temp sensor kitchen and salon
          
          char m[80];
          strcpy(m, sysStat);
          strcat(m, data);
          puts(m);
          smsSendStatus(senderNumber, m);
          smsClean();
        break;  
      }
      break;
  }
}

//SMS sending and reaciving
void smsRecive()
{
  if (sms.available())
  {
    sms.remoteNumber(senderNumber, 20);
  }
  if (sms.peek() == ',')
  {
    for (int i = 0; i < sizeOfSafeNumbers; i++)
    {
      if (!strcmp(senderNumber, safeNumbers[i]))
      {
        for (int j = 0; j < 3; j++)
        {
          smsText[i] = sms.read();
        }
      }
    }
    sms.flush();
  }
  sms.flush();
}
void smsSend(char senderNumber, byte noMessage)
{
  if (!strcmp(senderNumber, safeNumbers[0]))
  {
    sms.beginSMS(senderNumber);
    sms.print(messages[noMessage]);
    sms.endSMS();
  }
  else
  {
    sms.beginSMS(senderNumber);
    sms.print(messages[noMessage]);
    sms.endSMS();
    delay(500);
    sms.beginSMS(safeNumbers[0]);
    sms.print(messages[noMessage]);
    sms.endSMS();
  }
}
void smsSendStatus(char senderNumber, char message)
{
    sms.beginSMS(senderNumber);
    sms.print(message);
    sms.endSMS();
}

//sms data clean
void smsClean()
{
  smsText[0] = 0;
  smsText[1] = 0;
  smsText[2] = 0;
  for (int i = 0; i < 20; i++)
  {
    senderNumber[i] = 0;
  }
}
